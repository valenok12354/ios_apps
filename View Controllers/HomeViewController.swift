//
//  HomeViewController.swift
//  TwoViews
//
//  Created by Igor Koiv on 09.07.2020.
//  Copyright © 2020 Igor Koiv. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    @IBOutlet weak var whatsuppButton: UIButton!
    @IBOutlet weak var adressLabel: UILabel!
    @IBOutlet weak var welcomeLabel: UILabel!
    let fullScreenImage = UIImageView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Utilities.addBackgoundImage(imageView: fullScreenImage, index: 1)
        view.addSubview(fullScreenImage)
        Utilities.setBlurEffect(view: self.view)
        view.addSubview(whatsuppButton)
        view.addSubview(adressLabel)
        view.addSubview(welcomeLabel)
        
    }
    
    @IBAction func whatsappButtonPressed(_ sender: Any) {
        let whatsappURL = URL(string: "https://api.whatsapp.com/send?phone=+79159856434")
        if UIApplication.shared.canOpenURL(whatsappURL!) {
            UIApplication.shared.open(whatsappURL!)
        }
        if UIApplication.shared.canOpenURL(whatsappURL! as URL) {
            UIApplication.shared.open(whatsappURL! as URL)
        } else { return
            
        }
    }
}

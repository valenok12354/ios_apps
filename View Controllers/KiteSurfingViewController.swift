//
//  KiteSurfingViewController.swift
//  TwoViews
//
//  Created by Igor Koiv on 31.08.2020.
//  Copyright © 2020 Igor Koiv. All rights reserved.
//
import UIKit

class KiteSurfingViewController: UIViewController {
    var kiteImage = UIImageView()
    var fullScreenImage = UIImageView()
    var kiteLabel = UILabel()
    @IBOutlet weak var buttonWithUs: UIButton!
    
    func setUpElements()  {
        Utilities.styleFilledButton(buttonWithUs)
    }
    
    override func viewWillAppear(_ animated: Bool) {
       super.viewWillAppear(animated)
       navigationController?.setNavigationBarHidden(false, animated: animated)
       Utilities.addCustomizedBackBtn(self.navigationController, self.navigationItem)
       navigationController?.navigationBar.alpha = 0.5
   }

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpElements()
        kiteImage.frame = CGRect(x: 10, y: 80, width: 169, height: 210)
        DownloadFirebaseHelper.imageFireStore(0, kiteImage)
        Utilities.addBackgoundImage(imageView: fullScreenImage, index: 0)
        view.addSubview(fullScreenImage)
        Utilities.setBlurEffect(view: self.view)
        view.addSubview(kiteImage)
        view.addSubview(buttonWithUs)
        Utilities.surfLalabelAdd(view: view)
    }
    
    @IBAction func autorizationSeague(_ sender: Any) {
        func tappedButton(sender: UIButton!) {
            performSegue(withIdentifier: "AuthSegue", sender: nil)
        }
    }
}



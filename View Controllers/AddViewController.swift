//
//  AddViewController.swift
//  TwoViews
//
//  Created by Igor Koiv on 10.07.2020.
//  Copyright © 2020 Igor Koiv. All rights reserved.
//

import UIKit
import AVKit

class AddViewController: UIViewController {
    
    var videoPlayer: AVPlayer?
    
    var videoPlayerLayer: AVPlayerLayer?
    
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var logInButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpElements()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Utilities.addCustomizedBackBtn(self.navigationController, self.navigationItem)
        setUpVideo()
    }
    
    func setUpElements()  {
        Utilities.styleFilledButton(signUpButton)
        Utilities.styleHollowButton(logInButton)
    }
    
    func setUpVideo() {
        // get the path to the resourse in the bundle
        let bundlePath = Bundle.main.path(forResource: "snowKite", ofType: "mp4")
        
        guard bundlePath != nil else {
            return
        }
        // create URL from it
        let url = URL(fileURLWithPath: bundlePath!)
        // create the video player item
        let item = AVPlayerItem(url: url)
        // create the player
        videoPlayer = AVPlayer(playerItem: item)
        // create the layer
        videoPlayerLayer = AVPlayerLayer(player: videoPlayer!)
        // adjust the size and frame
        videoPlayerLayer?.frame = CGRect(x: -self.view.frame.size.width, y: 0, width: self.view.frame.size.width*3, height: self.view.frame.size.height)
        view.layer.insertSublayer(videoPlayerLayer!, at: 0)
        // add it to the view and play it
        videoPlayer?.playImmediately(atRate: 0.3)
    }
    
    @IBAction func SignUpSegue(_ sender: Any) {
        func tappedButton(sender: UIButton!) {
            performSegue(withIdentifier: "SignUpSegue", sender: nil)
        }
    }
    
    @IBAction func LogInSeague(_ sender: Any) {
        func tappedButton(sender: UIButton!) {
            performSegue(withIdentifier: "LogInSeague", sender: nil)
        }
    }
}

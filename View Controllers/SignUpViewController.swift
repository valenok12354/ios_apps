//
//  SignUpViewController.swift
//  TwoViews
//
//  Created by Igor Koiv on 09.07.2020.
//  Copyright © 2020 Igor Koiv. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseFirestore

class SignUpViewController: UIViewController {
 
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        
        Utilities.addCustomizedBackBtn(self.navigationController, self.navigationItem)
        setUpElements()
        view.addGestureRecognizer(tap)
    }
    
    @IBOutlet weak var firstNameTextField: UITextField!
    
    @IBOutlet weak var secondNameTextField: UITextField!
    
    @IBOutlet weak var emailField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var signUpButton: UIButton!
    
    @IBOutlet weak var errorLabel: UILabel!
    
    func setUpElements()  {
        errorLabel.alpha=0
        Utilities.styleTextField(firstNameTextField)
        Utilities.styleTextField(secondNameTextField)
        Utilities.styleTextField(emailField)
        Utilities.styleTextField(passwordTextField)
        Utilities.styleFilledButton(signUpButton)
    }
    
    //Validates fields and if everything is ok, returns nil otherwise it returns error message
    func validateFields() -> String? {
        
        //check the fields are all filled in
        if firstNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            secondNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            emailField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            return "Пожалуйста, заполните все поля"
        }
        // check if the password is secure
        let cleanedPassword = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if Utilities.isPasswordValid(cleanedPassword) == false {
            // password isn't secure enough
            return "Пожалуйста, введите хороший пароль"
        }
        
        return nil
    }
    
    @IBAction func touchAction(_ sender: Any) {
        
        //validate fields
        let error = validateFields()
        if error != nil {
            //something wrong with the fields, show error message
            showErrorMessage(error!)
        }
        else {
            //create clean versions of the data
            let firstName = firstNameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let lastName = secondNameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let email = emailField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let password = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            //create a user
            Auth.auth().createUser(withEmail: email, password: password) { (result, err) in
                if err != nil {
                    // there was an error creating a user
                    self.showErrorMessage("Не удалось создать запись, пожалуйста введите хороший пароль")
                }
                else {
                    // user was created sucsessfully
                    let db = Firestore.firestore()
                    db.collection("users").addDocument(data: ["firstName": firstName, "lastName": lastName, "uid": result!.user.uid ]) { (error) in
                        if error != nil {
                            // there was an error, show err message
                            self.showErrorMessage("Данные нельзя сохранить в базу данных")
                            
                        }
                    }
                    //transition to the home screen
                    self.transitionToHome()
                }
            }
            
        }
        
    }
    
    func showErrorMessage(_ message:String)  {
        errorLabel.text = message
        errorLabel.alpha = 1
    }
    func transitionToHome()  {
        let homeViewController = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.homeViewController) as? HomeViewController
        homeViewController?.modalPresentationStyle = .automatic
        self.present(homeViewController!, animated: true, completion: nil)
        
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

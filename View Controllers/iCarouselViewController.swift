import UIKit
import iCarousel
import Firebase

class iCarouselViewController: UIViewController, iCarouselDataSource, iCarouselDelegate {
    @IBOutlet weak var fullScreenImage: UIImageView!
    let descriptionLabel = UILabel()
    let myCarousel: iCarousel = {
        let view = iCarousel()
        view.type = .rotary
        return view
        
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        Utilities.addCustomizedBackBtn(self.navigationController, self.navigationItem)
        tabBarController?.tabBar.alpha = 0.5
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fullScreenImage.contentMode = .scaleAspectFill
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        fullScreenImage.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        Utilities.setBlurEffect(view: self.view)
        let db = Firestore.firestore()
        DownloadFirebaseHelper.getSurfDescription(db)
        Utilities.styleAlohaLabel(descriptionLabel)
        setUpCarousel(carousel: myCarousel)
        view.addSubview(descriptionLabel)
        view.addSubview(Utilities.logoImageSetUp())
        view.addSubview(myCarousel)
    }
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        return 3
    }
    
    func setUpCarousel(carousel: iCarousel) -> iCarousel {
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(gestureFired(gesture:)))
        gestureRecognizer.numberOfTapsRequired = 1
        carousel.isUserInteractionEnabled = true
        carousel.contentView.addGestureRecognizer(gestureRecognizer)
        carousel.dataSource = self
        carousel.delegate = self
        carousel.frame = CGRect(x: 0, y: 170, width: view.frame.size.width, height: 500)
        carousel.decelerationRate = 0.0;
        return carousel
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 290, height: 390))
        let imageView = UIImageView(frame: view.bounds)
        imageView.contentMode = .scaleToFill
        
        DownloadFirebaseHelper.imageFireStore(index, imageView)
        
        imageView.layer.cornerRadius = 12
        imageView.clipsToBounds = true
        Utilities.imageShadowing(image: imageView)
        view.addSubview(imageView)
        
        return view
    }
    
    func carouselDidEndScrollingAnimation(_ carousel: iCarousel) {
        switch carousel.currentItemIndex {
        case 0:
            DownloadFirebaseHelper.imageFireStore(0, fullScreenImage)
        case 1:
            DownloadFirebaseHelper.imageFireStore(1, fullScreenImage)
        case 2:
            DownloadFirebaseHelper.imageFireStore(2, fullScreenImage)
        default:
            view.backgroundColor = .blue
        }
        print(carousel.currentItemIndex)
    }
    
    @objc func gestureFired(gesture: UITapGestureRecognizer) {
        let iconIndex = myCarousel.currentItemIndex
        switch iconIndex {
        case 0:
            performSegue(withIdentifier: "ToKiteSurfSegue", sender: nil)
        case 1:
            performSegue(withIdentifier: "ToLogInSegue", sender: nil)
        case 2:
            performSegue(withIdentifier: "ToLogInSegue", sender: nil)
        default:
            print("An error occured")
        }
    }
}

//
//  PostViewController.swift
//  SurfPoint
//
//  Created by Igor Koiv on 28.09.2020.
//  Copyright © 2020 Igor Koiv. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class PostViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    var tableView: UITableView!
    var posts = [ //Post]()
            Post(id: "1", author: "Cheburashka", text: "I'am stange"),
            Post(id: "2", author: "Leopold", text: "Давайте жить дружно, ну что вы уж ваще"),
            Post(id: "3", author: "Karlson", text: "В меру упитанный мужчина в самом рассвете сил, спокойствие! Только спокойствие!")
        ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView = UITableView (frame: view.bounds, style: .plain)
        let cellNib = UINib(nibName: "PostTableViewCell", bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: "postCell")
        
        view.addSubview(tableView)
        var layoutGuide: UILayoutGuide!
        layoutGuide = view.safeAreaLayoutGuide
        tableView.leadingAnchor.constraint(equalTo: layoutGuide.leadingAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: layoutGuide.topAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: layoutGuide.trailingAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: layoutGuide.bottomAnchor).isActive = true
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.reloadData()
//        observePosts()
        
    }
    
//    func observePosts() {
//        let postRef = Database.database().reference().child("post")
//        postRef.observe(.value, with: {snapshot in
//            var tempPost = [Post]()
//
//            for child in snapshot.children {
//                if let childSnapshot = child as? DataSnapshot,
//                    let dict = childSnapshot.value as? [String:Any],
//                    let author = dict["author"] as? String,
//                    let text = dict["text"] as? String {
//                    let post = Post(id: childSnapshot.key, author: author, text: text)
//                    tempPost.append(post)
//                }
//            }
//            self.posts = tempPost
//            self.tableView.reloadData()
//        })
//
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "postCell", for: indexPath) as! PostTableViewCell
        cell.set(post: posts[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}

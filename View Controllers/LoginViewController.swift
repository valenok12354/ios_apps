//
//  LoginViewController.swift
//  TwoViews
//
//  Created by Igor Koiv on 09.07.2020.
//  Copyright © 2020 Igor Koiv. All rights reserved.
//

import UIKit
import FirebaseAuth

class LoginViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        Utilities.addCustomizedBackBtn(self.navigationController, self.navigationItem)
        setUpElements()
        view.addGestureRecognizer(tap)
    }
    
    func setUpElements() {
        errorLabel.alpha=0
        
        Utilities.styleTextField(emailTextField)
        Utilities.styleTextField(passwordTextField)
        Utilities.styleFilledButton(loginButton)
        
    }
    
   @objc func dismissKeyboard() {
          view.endEditing(true)
      }
    
    @IBAction func loginTapped(_ sender: Any) {
        //TODO: validate text fields
        
        //Create cleaned versions of the pass and mail
        let email = emailTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let password = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        //signIn user
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            if error != nil {
                self.errorLabel.text = error?.localizedDescription
                self.errorLabel.alpha = 1
            }
            else {
                
                let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.homeViewController) as? HomeViewController
                homeViewController?.modalPresentationStyle = .automatic
                self.present(homeViewController!, animated: true, completion: nil)
            }

        }
    }
    
}

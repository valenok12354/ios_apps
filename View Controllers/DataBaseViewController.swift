//
//  DataBaseViewController.swift
//  SurfPoint
//
//  Created by Igor Koiv on 11.09.2020.
//  Copyright © 2020 Igor Koiv. All rights reserved.
//

import UIKit
import Firebase

class DataBaseViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        let db = Firestore.firestore()
        
        //Adding a document
        db.collection("wine").addDocument(data: ["year": 2018, "type": "pino-grigio", "label": "Abrau-Durso"])
    }
}

//
//  PostTableViewCell.swift
//  SurfPoint
//
//  Created by Igor Koiv on 28.09.2020.
//  Copyright © 2020 Igor Koiv. All rights reserved.
//

import UIKit

class PostTableViewCell: UITableViewCell {

    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var postTextLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        profileImageView.layer.cornerRadius = profileImageView.bounds.height/2
        profileImageView.clipsToBounds = true
        DownloadFirebaseHelper.imageFireStore(1, profileImageView)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func set(post: Post) {
        userNameLabel.text = post.author
        postTextLabel.text = post.text
        
    }
    
}

//
//  Post.swift
//  SurfPoint
//
//  Created by Igor Koiv on 28.09.2020.
//  Copyright © 2020 Igor Koiv. All rights reserved.
//

import Foundation

class Post {
    var id: String
    var author: String
    var text: String
    
    init(id:String, author:String, text:String) {
        self.id = id
        self.author = author
        self.text = text
        
    }
    
}

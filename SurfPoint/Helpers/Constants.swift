//
//  Constants.swift
//  TwoViews
//
//  Created by Igor Koiv on 15.07.2020.
//  Copyright © 2020 Igor Koiv. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    struct Storyboard {
        static let homeViewController = "HomeVC"
        static let addViewController = "AddVC"
        static let signUpViewController = "SignUpVC"
        static let loginViewController = "LoginVC"
        static let iCarouselViewController = "iCarouselVC"
    }
    static let leftDistanceToView: CGFloat = 40
    static let rightDistanceToView: CGFloat = 40
    static let galleryMinimumLineSpacing: CGFloat = 20
    static let galleryItemWidth = (UIScreen.main.bounds.width - Constants.leftDistanceToView - Constants.rightDistanceToView - (Constants.galleryMinimumLineSpacing / 2)) / 2
}

//
//  Utilities.swift
//  customauth
//
//  Created by Christopher Ching on 2019-05-09.
//  Copyright © 2019 Christopher Ching. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class Utilities {
    
    static func styleTextField(_ textfield:UITextField) {
        
        // Create the bottom line
        let bottomLine = CALayer()
        
        bottomLine.frame = CGRect(x: 0, y: textfield.frame.height - 2, width: textfield.frame.width-40, height: 2)
        
        bottomLine.backgroundColor = UIColor.init(red: 30/255, green: 144/255, blue: 255/255, alpha: 1).cgColor
        
        // Remove border on text field
        textfield.borderStyle = .none
        
        // Add the line to the text field
        textfield.layer.addSublayer(bottomLine)
        
    }
    
    static func styleFilledButton(_ button:UIButton) {
        
        // Filled rounded corner style
        button.backgroundColor = UIColor.init(red: 30/255, green: 144/255, blue: 255/255, alpha: 1)
        button.layer.cornerRadius = 25.0
        button.tintColor = UIColor.white
    }
    
    static func styleHollowButton(_ button:UIButton) {
        
        // Hollow rounded corner style
        button.layer.borderWidth = 2
        button.layer.borderColor = UIColor.init(red: 30/255, green: 144/255, blue: 255/255, alpha: 1).cgColor
        button.layer.cornerRadius = 25.0
        button.tintColor = UIColor.init(red: 30/255, green: 144/255, blue: 255/255, alpha: 1)
    }
    
    static func isPasswordValid(_ password : String) -> Bool {
        
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^([a-zA-Z0-9]+$)")
        return passwordTest.evaluate(with: password)
    }
    
    static func addCustomizedBackBtn(_ navigationController: UINavigationController?, _ navigationItem: UINavigationItem?) {
        navigationItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    static func styleAlohaLabel(_ label:UILabel){
        
        label.frame = CGRect(x: 10, y: 130, width: 370, height: 55)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.text = "АЛОХА!  МЫ СТАНЦИЯ SURF POINT"
        label.textColor = .white
        label.font = UIFont (name: "IntroDemo-BlackInlineCAPS", size: 25)
        label.layer.shadowRadius = 15
        label.layer.shadowOpacity = 0.4
        label.sizeToFit()
    }
    
    static func logoImageSetUp() -> UIImageView {
        var logoImage: UIImageView?
        let image: UIImage = UIImage(named: "logo")!
        logoImage = UIImageView(image: image)
        logoImage!.frame = CGRect(x: 20, y: 50, width: 200, height: 55)
        return logoImage!
        
    }
    
    static func imageShadowing(image: UIImageView) {
        image.layer.shadowColor = UIColor.gray.cgColor
        image.layer.shadowOffset = CGSize(width: 10, height: 10)
        image.layer.shadowOpacity = 0.5
        image.layer.shadowRadius = 10
        image.layer.shouldRasterize = true
    }
    
    static func surfLalabelAdd(view: UIView) {
        let db = Firestore.firestore()
        let kiteLabel = UILabel.init()
        kiteLabel.frame = CGRect(x: 10, y: 300, width: view.frame.size.width-20, height: 230)
        kiteLabel.textAlignment = NSTextAlignment.center
        kiteLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        kiteLabel.numberOfLines = 0
        kiteLabel.textColor = .white
        kiteLabel.font = UIFont(name: "IntroDemo-BlackCAPS", size: 15.0)
        kiteLabel.text = DownloadFirebaseHelper.getSurfDescription(db)
        view.addSubview(kiteLabel)
    }
    
    static func addBackgoundImage(imageView: UIImageView, index: Int) {
        imageView.contentMode = .scaleAspectFill
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        imageView.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        DownloadFirebaseHelper.imageFireStore(index, imageView)
    }
    
    static func setBlurEffect(view: UIView) {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(blurEffectView)
    }
}

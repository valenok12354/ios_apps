//
//  DownloadImageHelper.swift
//  SurfPoint
//
//  Created by Igor Koiv on 18.09.2020.
//  Copyright © 2020 Igor Koiv. All rights reserved.
//

import Foundation
import FirebaseStorage
import SDWebImage
import Firebase

class DownloadFirebaseHelper {
    static var documentData = String()
    static func imageFireStore(_ index: Int, _ imageView: UIImageView) {
        let storage = Storage.storage()
        var reference: StorageReference!
        reference = storage.reference(forURL: "gs://surfpoint-2020.appspot.com")
        let imageRef = reference.child("images/surf_\(index).pdf")
        imageRef.downloadURL { (url, _) in
            guard let downURL = url else {
                print("Error")
                return
            }
            imageView.sd_setImage(with: downURL, completed: nil)
        }
    }
    
   static func getSurfDescription(_ db: Firestore) -> String {
    
        //        let newDoc = db.collection("wine").addDocument(data: ["test": 123])
        
        //        db.collection("wine").addDocument(data: ["year": 2018, "type": "pino-grigio", "label": "Abrau-Durso", "id": newDoc.documentID])
        //        db.collection("surf").document("surfTypeDescription").setData(["kite": "testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttest"])
        
        db.collection("surf").document("surfTypeDescription").getDocument { (document, error) in
            if error == nil {
                if document != nil && document!.exists {
                    documentData = document?.data()!["kite"] as! String
                }
            }
        }
    return documentData
    }
}
